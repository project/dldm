<?php
/**
 * Created by PhpStorm.
 * User: podarokua
 * Date: 23.12.13
 * Time: 19:00
 */

namespace Drupal\dldm;


class DomainLocationStorage {

  /**
   * @var string
   * loc_id location id
   * primary key
   */
  private $id;

  /**
   * @var string
   * Domain or any Entity field name from storage database linked with Location
   * usually = $linkedField
   */
  private $domainId;

  /**
   * @var DomainLocationItem
   * @tofo looks like we need just array here
   */
  private $item;

  /**
   * @var string
   */
  private $module;

  /**
   * @var array
   */
  private $schema;

  /**
   * @var string
   */
  private $table;

  /**
   * @var array
   */
  private $defaultFields;

  /**
   * @var array
   */
  private $defaultValues;

  /**
   * @var string
   * linked table name
   */
  private $linkedTable;

  /**
   * @var string
   * linked field name
   * usually = $domainId
   */
  private $linkedField;

  /**
   * @param string $module
   * @param string $schemaName
   */
  public function __construct($module = 'dldm', $schemaName = 'domain_location') {
    $this->module = $module;
    $this->table = $schemaName;
    $this->setSchema($this->table);
    // get primary key for feature db_update
    // @todo possible fix for combined primary keys
    $this->id = $this->schema[$this->table]['primary key'][0];
    // @todo possible fix for combined unique keys
    $this->domainId = array_values($this->schema[$this->table]['unique keys'])[0][0];
    unset($this->schema[$this->table]['fields'][$this->id]);
    $this->defaultFields = array_keys($this->schema[$this->table]['fields']);
    // parse schema
    $values = array_values($this->schema[$this->table]['fields']);
    foreach ($values as $field => $fieldSchema) {
      $this->defaultValues[$field] = isset($fieldSchema['default']) ? $fieldSchema['default'] : is_null($fieldSchema['not null'] ? NULL : 1);
    }
    $this->defaultValues = array_values($this->defaultValues);
    // setting link to table for searching linked entity ID
    $this->setLink();
  }

  /**
   * @param $schemaName string
   * @return $this
   */
  private function setSchema($schemaName) {
    module_load_install($this->module);
    $fullSchema = call_user_func($this->module . '_schema');
    $this->schema = array($this->table => $fullSchema[$schemaName]);
    return $this;
  }

  public function setLink($table = 'domain', $field = 'subdomain') {
    $this->linkedTable = $table;
    $this->linkedField = $field;
  }

  /**
   * @param DomainLocationItem $item
   * @return $this
   */
  public function setItem(DomainLocationItem $item) {
    $this->item = $item;
    return $this;
  }


  /**
   * @return $this
   * @todo fix duplicates
   */
  public function save() {
    // @check for duplicates
    $check = $this->get($this->item->getId());
    if (empty($check)) {
// get domainId unique?
      $query = db_select($this->table, 'c');
      $query->fields('c');
      $query->condition($this->domainId, $this->item->getUpdating()[$this->domainId], '=');
      $check = $query->execute()->fetchAssoc();
      if ((empty($check))) {
        db_insert($this->table)
          ->fields($this->item->getFields(), $this->item->getValues())
          ->execute();
        return $this;
      }
    }
    $this->update();
  }

  /**
   * @return $this
   */
  public function update() {
    db_update($this->table)
      ->fields($this->item->getUpdating($this->id))
      ->condition($this->id, $this->item->getId(), '=')
      ->execute();
    return $this;
  }

  /**
   * @param $id
   * @return $this
   */
  public function delete($id) {
    db_delete($this->table)->condition($this->id, $id, '=')->execute();
    return $this;
  }

  /**
   * @param $id int
   * location id 'loc_id'
   * @return mixed
   */
  public function get($id) {
    $query = db_select($this->table, 'c');
    $query->fields('c');
    $query->condition('loc_id', $id, '=');
    $item = $query->execute()->fetchAssoc();
    return $item;
  }

  // @todo do we need this?
  public function getDomainId(DomainLocationItem $item) {
    return $item->getUpdating()[$this->domainId];
  }

  /**
   * use this method for obtaining item skeleton from table schema
   * removed primary key ( loc_id ) from it
   * @return array
   */
  public function getEmptyItem() {
    return array_combine($this->defaultFields, $this->defaultValues);
  }

  /**
   * @param $domainId string|int
   * can be real ID of linked entity
   * or string for searching ID in linked table
   * @return \DatabaseStatementInterface|int|null
   */
  public function newItem($domainId = 1) {
    $item = $this->getEmptyItem();
    // @todo fix this garbage
    $item['lat'] = (float) NULL;
    $item['lng'] = (float) NULL;
    if (empty($domainId)) {
      // something wrong, $domainId cannot be empty
      return FALSE;
    }
    if (!is_int($domainId) && !is_null($domainId)) {
      // got string for searching
      // fill linked domainId
      $query = db_select($this->linkedTable, 'd');
      $query->fields('d', array($this->domainId));
      $query->condition($this->linkedField, $domainId);
      $item[$this->domainId] = $query->execute()->fetchCol()[0];
      return $item;
    }
    $item[$this->domainId] = $domainId;
    return $item;
  }
}
