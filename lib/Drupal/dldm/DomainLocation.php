<?php
/**
 * @file
 * Contains DomainLocation.php
 */

namespace Drupal\dldm;


class DomainLocation {

  /**
   * @var static Singleton instance
   */
  static private $dldmInstance;

  /**
   * @var static domains array
   */
  static private $dldmDomains;

  /**
   * do not change
   */
  private function __construct() {}

  /**
   * do not clone
   */
  protected function __clone() {}

  /**
   * @return DomainLocation SingleTon instance
   */
  static public function getInstance() {
    if (is_null(self::$dldmInstance)) {
      self::$dldmInstance = new self();
    }
    return self::$dldmInstance;
  }

  /**
   * @param array $domains
   * @return array $row
   */
  public function FillDomainLocations(array $domains){
    if (!isset(self::$dldmDomains[$domains['domain_id']])){
      // get all the data from table
      // @todo remove json column from here
    $row = db_select('domain_location', 'dl')
      ->fields('dl')
      ->condition('domain_id', $domains['domain_id'], '=')->execute()->fetchAssoc();
      if (!is_null((array)$row)){
      self::$dldmDomains[] = (array)$row;
      }
    }
    if (isset(self::$dldmDomains[$domains['domain_id']])){
    return self::$dldmDomains[$domains['domain_id']];
    }
  }

  /**
   * @param $address string
   */
  public function GetGoogleLocation($address){

  }

  private function GetDomainId(array $domains){
    return db_select('domain', 'dl')->fields('dl', array('domain_id', 'subdomain'))->condition('subdomain', $domains['subdomain'], '=')->execute()->fetchField();
  }

  public function SaveDomainLocations(array $domains){
    $domains['domain_id'] = is_null($domains['domain_id']) ? $this->GetDomainId($domains) : $domains['domain_id'];
    unset($domains['subdomain']);
    db_update('domain_location')->fields($domains)->execute();
  }

  public function DeleteDomainLocations(array $domains){
    print_r($this->GetDomainId($domains), TRUE);
    //db_delete('domain_location')->condition('domain_id', $domains['domain_id'], '=');
  }
} 