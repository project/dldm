<?php
/**
 * Created by PhpStorm.
 * User: podarokua
 * Date: 23.12.13
 * Time: 18:54
 */

namespace Drupal\dldm;


class DomainLocationItem {

  /**
   * @var array
   */
  private $item;

  private $storage;

  /**
   * @var int
   * Id from loc_id column
   */
  private $id;

  /**
   * @param DomainLocationStorage $storage
   * @param $domainId int|string
   */
  public function __construct(DomainLocationStorage $storage, $domainId){
    $this->storage = $storage;
    $this->item = $this->storage->newItem($domainId);
    $this->id = $this->getId();
  }

  public function setAddress($user_address = 'Ukraine'){
    $this->item['user_address'] = $user_address;
    $this->geocode();
    return $this;
  }

  /**
   * @param $id
   * @return array
   */
  public function get($id){
    $this->item = $this->storage->get($id);
    return $this->item;
  }

  /**
   * @return array
   * array prepared for db_update fields
   */
  public function getFields() {
    return array_keys($this->item);
  }

  /**
   * @return array
   * array prepared for db_update values
   */
  public function getValues() {
    return array_values($this->item);
  }

  /**
   * Prepare item for db_update
   * unset $key from resulting array
   * @param string $key
   * @return array
   */
  public function getUpdating($key = 'loc_id') {
    $updating = $this->item;
    // check empty key
    if ($key != 'loc_id' && !is_null($key)) {
      unset($updating[$key]);
    }
    return $updating;
  }

  /**
   * Perform a geocode on a location array.
   */
  private function geocode(){

    // function google_geocode_location($location = array()) {

    // @todo language handling workaround
      $query = array(
        'address' => $this->item['user_address'],
        'sensor' => 'false',
        'language' => 'uk'
      );

      $url = url('http://maps.googleapis.com/maps/api/geocode/json', array(
        'query' => $query,
        'external' => TRUE,
      ));

      $http_reply = drupal_http_request($url);
      $data = json_decode($http_reply->data);

      $status_code = $data->status;
      if ($status_code != 'OK') {
        watchdog('location', 'Google geocoding returned status code: %status_code for the query url: %url', array(
          '%status_code' => $data->status,
          '%url' => $url
        ));
        return NULL;
      }
    $this->item['formatted_address'] = $data->results[0]->formatted_address;
    $this->item['lat'] = $data->results[0]->geometry->location->lat;
    $this->item['lng'] = $data->results[0]->geometry->location->lng;
    return $this;
  }

  public function getId(){
    return isset($this->item['loc_id']) ? $this->item['loc_id'] : NULL;
  }

  public function getItem(){
    return $this->item;
  }
}
