<?php
/**
 * Created by PhpStorm.
 * User: podarokua
 * Date: 23.12.13
 * Time: 18:49
 */

namespace Drupal\dldm;


class DomainLocationService {

  /**
   * @var static Singleton instance
   */
  static private $dldmInstance;

  /**
   * @var static domains array
   */
  private $storage;

  /**
   * just creates single $storage DomainLocationStorage
    */
  private function __construct() {
    $this->storage = new DomainLocationStorage('dldm', 'domain_location');
  }

  /**
   * do not clone
   */
  protected function __clone() {
  }

  /**
   * @return DomainLocation SingleTon instance
   */
  static public function getInstance() {
    if (is_null(self::$dldmInstance)) {
      self::$dldmInstance = new self();
    }
    return self::$dldmInstance;
  }

  public function getStorage(){
    return $this->storage;
  }

} 