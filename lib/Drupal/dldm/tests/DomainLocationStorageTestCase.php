<?php
/**
 * @file
 * Contains DomainLocationStorageTestCase.php
 */

namespace Drupal\dldm\Tests;

use Drupal\dldm\DomainLocationStorage;


class DomainLocationStorageTestCase extends DrupalUnitTestCase {
  /**
   * @var DomainLocationStorage
   */
  private $storage;

  public function setUp() {
    drupal_load('module', 'dldm');
    parent::setUp();
  }

  public static function getInfo() {
    return array(
      'name' => 'dldm module DomainLocationStorage unit tests',
      'description' => 'DomainLocationStorage API',
      'group' => 'dldm',
    );
  }

  public function testDomainLocationStorage() {
    $this->storage = new DomainLocationStorage();
    drupal_install_schema('system');
    drupal_install_schema('domain');

    // check without any domain data in 'domain' schema
    $result = $this->storage->newItem(1);
    $check = array(
      'domain_id' => 1,
      'formatted_address' => FALSE,
      'user_address' => 'Ukraine',
      'lat' => 0,
      'lng' => 0,
      'json' => FALSE,
    );
    $this->assertEqual($result, $check);

    // check with entered domain
    $domain = array('domain_id' => 2, 'subdomain' => 'xxx.com', 'sitename' => 'test', 'machine_name' => 'test');
    $query = db_insert('domain');
    $query->fields(array_keys($domain), array_values($domain));
    $query->execute();

    // check with entered domain via subdomain
    $result = $this->storage->newItem('xxx.com');
    $check = array(
      'domain_id' => '2',
      'formatted_address' => FALSE,
      'user_address' => 'Ukraine',
      'lat' => 0,
      'lng' => 0,
      'json' => FALSE
    );
    $this->assertEqual($result, $check);

    // check with entered domain via domain_id
    $result = $this->storage->newItem(2);
    $check = array(
      'domain_id' => 2,
      'formatted_address' => FALSE,
      'user_address' => 'Ukraine',
      'lat' => 0,
      'lng' => 0,
      'json' => FALSE,
    );
    $this->assertEqual($result, $check);

    // check with entered domain via unknown domain_id
    $result = $this->storage->newItem(3);
    $check = array(
      'domain_id' => 2,
      'formatted_address' => FALSE,
      'user_address' => 'Ukraine',
      'lat' => 0,
      'lng' => 0,
      'json' => FALSE,
    );
    $this->assertEqual($result, $check);

    $this->assert('debug', '<pre>' . print_r($this->storage, TRUE) . '</pre>');
  }
}
