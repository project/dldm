<?php
/**
 * @file
 * Contains DomainLocationServiceTestCase.php
 */

namespace Drupal\dldm\Tests;

use Drupal\dldm\DomainLocationService;


class DomainLocationServiceTestCase extends DrupalUnitTestCase{

  public function setUp(){
    drupal_load('module', 'dldm');
    parent::setUp();
  }

  public static function getInfo(){
    return array(
      'name' => 'dldm module DomainLocationService class unit tests',
      'description' => 'DomainLocationService API',
      'group' => 'dldm',
    );
  }

  public function testDomainLocationService(){
    $storage = DomainLocationService::getInstance()->getStorage();

    $result = get_class($storage);
    $check = 'Drupal\\dldm\\DomainLocationStorage';
    $this->assertEqual($result, $check);
  }

}
